
# GLOSSARIES

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
    print "$_[0]\n";
	my ($base_name, $path) = fileparse( $_[0] );
	pushd $path;
	#my $return = system "makeglossaries $base_name";
    print "$base_name\n";

    if ( $silent ) {
        system "makeglossaries -q '$base_name'";
    }
    else {
        system "makeglossaries '$base_name'";
    };

	popd;
}

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';

# INDEX

add_cus_dep("nlo", "nls", 0, "nlo2nls");

sub nlo2nls {
    system("makeindex $_[0].nlo -s nomencl.ist -o $_[0].nls -t $_[0].nlg");
}

# CLEANING UP

$clean_ext .= ' %R.ist %R.xdy %R.bbl %R.d %R.nav %R.run.xml %R.snm'

