OUT=output
NAME=talk
DIR=$(shell basename ${PWD})

export max_print_line=1000

all: latex view

publish: latex
	git -am "publishing"
	cp $(OUT)/$(NAME).pdf ../$(DIR).pdf

latex:
	latexmk -silent -lualatex -auxdir=$(OUT) -outdir=$(OUT) -M -MP -MF $(OUT)/$(NAME).d $(NAME).tex

test: clean
	latexmk -lualatex -auxdir=$(OUT) -outdir=$(OUT) -M -MP -MF $(OUT)/$(NAME).d $(NAME).tex

view: 
	@nohup evince $(OUT)/$(NAME).pdf&

clean: 
	@latexmk -silent -outdir=$(OUT) -C

.PHONY: all clean
